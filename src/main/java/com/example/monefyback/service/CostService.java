package com.example.monefyback.service;

import com.example.monefyback.domain.Category;
import com.example.monefyback.domain.Cost;
import com.example.monefyback.exception.NotFoundException;
import com.example.monefyback.model.CalculusDto;
import com.example.monefyback.repository.CategoryRepository;
import com.example.monefyback.repository.CostRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class CostService {

    private final CostRepository costRepository;
    private final CategoryRepository categoryRepository;
    private final ObjectMapper objectMapper;

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    Validator validator = factory.getValidator();

    public Double addCost(CalculusDto calculusDto){
        Set<ConstraintViolation<CalculusDto>> violations = validator.validate(calculusDto);
        if (violations.size() > 1) {
            StringBuilder stringBuilder = new StringBuilder();
            violations.forEach(validateError -> stringBuilder.append(validateError.getMessage()) );
            throw new RuntimeException(stringBuilder.toString());
        }
        Optional<Category> categoryOptional = categoryRepository.findById(calculusDto.getCategoryId());
        if (categoryOptional.isPresent()){
            Category category = categoryOptional.get();
            Cost cost = Cost.builder()
                    .cost(calculusDto.getCalculus())
                    .createdAt(LocalDate.now())
                    .note(calculusDto.getNote())
                    .category(category)
                    .build();
           return costRepository.save(cost).getCost();
        }
        throw new NotFoundException(String.format("Could not found category by %s id", calculusDto.getCategoryId()));
    }

    public List<Cost> interval(String starDate, String endDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate starLocalDate = LocalDate.parse((starDate), formatter);
        LocalDate endLocalDate = LocalDate.parse((endDate), formatter);
        return objectMapper.convertValue(costRepository.findByInterval(starLocalDate, endLocalDate), new TypeReference<>() {
        });
    }
    public List<Cost> findByDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate= LocalDate.parse((date), formatter);
        return objectMapper.convertValue(costRepository.findByDate(localDate), new TypeReference<>() {
        });
    }
    public Double getSumCost(){

        return costRepository.getSumCost();

    }
    public List<Cost> getByMonth(Integer monthNum){
        return objectMapper.convertValue(costRepository.findByMonth(monthNum), new TypeReference<>() {
        });
    }

}
