package com.example.monefyback.service;

import com.example.monefyback.domain.register.Role;
import com.example.monefyback.domain.register.User;
import com.example.monefyback.model.register.LoginDto;
import com.example.monefyback.model.register.RegisterDto;
import com.example.monefyback.repository.register.RoleRepository;
import com.example.monefyback.repository.register.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final AuthenticationManager authenticationManager;

    public ResponseEntity<?> registerUser(RegisterDto registerDto){
        if (userRepository.existsByUsername(registerDto.getUsername())){
            return new ResponseEntity<>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }
        if (userRepository.existsByEmail(registerDto.getEmail())){
            return new ResponseEntity<>("Email is already taken!", HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setUsername(registerDto.getUsername());
        user.setEmail(registerDto.getEmail());
        user.setCreatedAt(LocalDate.now());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));

        Optional<Role> roleOptional = roleRepository.findByRoleName("ROLE_ADMIN");
        if (roleOptional.isPresent()) {
            Role role = roleOptional.get();

            user.setRoles(Collections.singleton(role));

            userRepository.save(user);

            return new ResponseEntity<>("User registered successfully", HttpStatus.OK);
        }
        return new ResponseEntity<>("Role not found!", HttpStatus.NOT_FOUND);
    }
    public ResponseEntity<?> authenticateUser(LoginDto loginDto){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginDto.getUsernameOrEmail(), loginDto.getPassword()
        ));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new ResponseEntity<>("User signedIn successfully!", HttpStatus.OK);
    }
}
