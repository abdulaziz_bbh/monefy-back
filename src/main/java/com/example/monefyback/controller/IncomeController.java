package com.example.monefyback.controller;

import com.example.monefyback.domain.Income;
import com.example.monefyback.model.CalculusDto;
import com.example.monefyback.service.IncomeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/income")
@RequiredArgsConstructor
public class IncomeController {

    private final IncomeService incomeService;

    @PostMapping("/add-income")
    private Double addCost(@RequestBody CalculusDto calculusDto){
        return incomeService.addIncome(calculusDto);
    }

    @GetMapping("/get-by-interval")
    private List<Income> getByInterval(@RequestParam String startDate, @RequestParam String endDate){
        return incomeService.interval(startDate, endDate);
    }
    @GetMapping("/get-by-date")
    private List<Income> getByDate(@RequestParam String date){
        return incomeService.findByDate(date);
    }
    @GetMapping("/get-sum")
    private Double getSum(){
        return incomeService.getSumIncome();
    }
    @GetMapping("/get-by-month")
    private List<Income> getByMonth(@RequestParam Integer monthNum){
        return incomeService.getByMonth(monthNum);
    }
}
