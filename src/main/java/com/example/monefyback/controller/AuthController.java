package com.example.monefyback.controller;

import com.example.monefyback.model.register.LoginDto;
import com.example.monefyback.model.register.RegisterDto;
import com.example.monefyback.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    @PostMapping("/signup")
    private ResponseEntity<?> registerUser(@RequestBody RegisterDto registerDto){
        return  authService.registerUser(registerDto);
    }
    @PostMapping("/signin")
    private ResponseEntity<?> authenticateUser(@RequestBody LoginDto loginDto){
        return authService.authenticateUser(loginDto);
    }
}
