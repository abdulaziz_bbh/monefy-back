package com.example.monefyback.controller;

import com.example.monefyback.model.register.RoleDto;
import com.example.monefyback.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @PostMapping("/add")
    private Long addRole(@RequestBody RoleDto dto){
        return roleService.addRole(dto);
    }

}
