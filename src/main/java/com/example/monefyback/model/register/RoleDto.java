package com.example.monefyback.model.register;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto {

    @NotNull(message = "Column cannot be null!")
    private String roleName;
}
