package com.example.monefyback.model.register;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginDto {

    @NotNull(message = "Column cannot be null!")
    private String usernameOrEmail;

    @NotNull(message = "Column cannot be null!")
    private String password;
}
