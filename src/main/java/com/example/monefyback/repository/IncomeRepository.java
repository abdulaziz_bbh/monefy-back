package com.example.monefyback.repository;

import com.example.monefyback.domain.Income;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.time.LocalDate;
import java.util.List;

public interface IncomeRepository extends JpaRepository<Income, Long> {

    @Query(value = "SELECT i FROM Income i WHERE  i.createdAt BETWEEN ?1 AND ?2")
    List<Income> findByInterval(LocalDate startDate, LocalDate endDate);

    @Query(value = "select i from Income i where i.createdAt = ?1")
    List<Income> findByDate(LocalDate date);

    @Query(value = "select sum(i.income) from Income i ")
    Double getSumIncome();

    @Query(value = "SELECT * FROM income WHERE EXTRACT(MONTH FROM created_at) = ?1", nativeQuery = true)
    List<Income> findByMonth(Integer monthNum);
}
